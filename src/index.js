import '../styles/global.css'
import * as Immutable from 'immutable';

const card = document.getElementsByTagName('div');
card[0].innerText = 'Functional Programming';

let student1 = {
    firstname: 'Othmane',
    note: 15
};
let student2 = {
    firstname: 'Othmane',
    note: 15
};

console.log('Object Original : ', student1);

// The Normal update 
student1.note = 16;
console.log('update note normally : ', student1);

// With immutable
let stud = Immutable.fromJS(student2);
let result = stud.set('note', 16);
console.log('update note with Immutable')
console.log('Updated Object : ', result.toJS());
console.log('Original Object : ', student2);